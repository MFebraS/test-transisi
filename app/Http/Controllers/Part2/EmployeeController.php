<?php

namespace App\Http\Controllers\Part2;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmployeeStoreRequest;
use App\Http\Requests\EmployeeUpdateRequest;
use App\Models\Company;
use App\Models\Employee;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['data'] = Employee::latest()->paginate(5);

        return view('part2.employee.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['companies'] = Company::orderBy('name')->get();

        return view('part2.employee.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  EmployeeStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeStoreRequest $request)
    {
        $post = $request->all();

        $employee = new Employee;
        $employee->name       = $post['name'];
        $employee->email      = $post['email'];
        $employee->company_id = $post['company_id'];
        $employee->save();

        $success = "Create employee success";

        return redirect()->route('employee.index')->withSuccess($success);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['data'] = Employee::findOrFail($id);

        return view('part2.employee.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['data'] = Employee::findOrFail($id);
        $data['companies'] = Company::orderBy('name')->get();

        return view('part2.employee.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  EmployeeUpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeUpdateRequest $request)
    {
        $post = $request->all();

        $employee = Employee::findOrFail($post['id']);
        $employee->name       = $post['name'];
        $employee->email      = $post['email'];
        $employee->company_id = $post['company_id'];
        $employee->save();

        $success = "Update employee success";

        return redirect()->route('employee.index')->withSuccess($success);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::findOrFail($id);
        $employee->delete();

        $success = "Delete employee success";

        return redirect()->route('employee.index')->withSuccess($success);
    }
}
