<?php

namespace App\Http\Controllers\Part2;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyStoreRequest;
use App\Http\Requests\CompanyUpdateRequest;
use App\Models\Company;
use File;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['data'] = Company::withCount('employees')->latest()->paginate(5);

        return view('part2.company.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('part2.company.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  CompanyStoreRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyStoreRequest $request)
    {
        $post = $request->all();

        $filename = $post['name'] . " - " . time() . "." . $post['logo']->getClientOriginalExtension();
        $path = $post['logo']->storeAs('company', $filename);

        $company = new Company;
        $company->name    = $post['name'];
        $company->email   = $post['email'];
        $company->logo    = $path;
        $company->website = $post['website'];
        $company->save();

        $success = "Create company success";

        return redirect()->route('company.index')->withSuccess($success);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['data'] = Company::with('employees')->findOrFail($id);

        return view('part2.company.detail', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['data'] = Company::findOrFail($id);

        return view('part2.company.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  CompanyUpdateRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyUpdateRequest $request)
    {
        $post = $request->all();

        $company = Company::findOrFail($post['id']);

        $old_image = $company->logo;     // old logo
        $path = $old_image;
        if (isset($post['logo'])) {
            $filename = $post['name'] . " - " . time() . "." . $post['logo']->getClientOriginalExtension();
            $path = $post['logo']->storeAs('company', $filename);

            if (File::exists( storage_path('app/' . $old_image) )) {
                // remove old image
                unlink(storage_path('app/' . $old_image));
            }
        }

        $company->name    = $post['name'];
        $company->email   = $post['email'];
        $company->logo    = $path;
        $company->website = $post['website'];
        $company->save();

        $success = "Update company success";

        return redirect()->route('company.index')->withSuccess($success);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $company = Company::withCount('employees')->findOrFail($id);
        
        // check employee
        if ($company->employees_count > 0) {
            $error = ['Can not delete '. $company->name .', because it has employee(s).'];

            return redirect()->route('company.index')->withErrors($error);
        }

        $path = storage_path('app/' . $company->logo);
        if (File::exists($path)) {
            // remove image
            unlink($path);
        }
        $company->delete();

        $success = "Delete company success";

        return redirect()->route('company.index')->withSuccess($success);
    }
}
