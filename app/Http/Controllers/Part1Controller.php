<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class Part1Controller extends Controller
{
	/* get average, 7 max, and 7 min from scores */
    public function calculateScore()
    {
    	$score_str = "72 65 73 78 75 74 90 81 87 65 55 69 72 78 79 91 100 40 67 77 86";
    	$scores = explode(' ', $score_str);

    	// get average score
    	$total_score = 0;
    	foreach ($scores as $key => $value) {
    		$total_score += $value;
    	}
    	$avg_score = $total_score / count($scores);

    	// get 7 max scores
    	$temp_scores = $scores;
    	rsort($temp_scores);
    	$max_scores = array_slice($temp_scores, 0, 7);

    	// get 7 min scores
    	sort($temp_scores);
    	$min_scores = array_slice($temp_scores, 0, 7);

    	$result = [
    		"score" 		=> $score_str,
    		"total_score" 	=> $total_score,
    		"score_average" => $avg_score,
    		"max_7_score" 	=> $max_scores,
    		"min_7_score" 	=> $min_scores,
    	];

    	return view('part1', ['result' => $result]);
    }

    /* count number of lowercase in string */
    public function calculateLowercase($string="TranSISI")
    {
    	$count = 0;

    	// get all lowercase letter
    	preg_match_all('/[a-z]/', $string, $matches);
    	$count = count($matches[0]);

    	$result = '"'. $string .'" mengandung '. $count .' buah huruf kecil';

        return view('part1', ['result' => $result]);
    }

    /* parse string into unigram, bigram, trigram */
    public function parseString($string="Jakarta adalah ibukota negara Republik Indonesia")
    {
    	$unigram = str_replace(' ', ', ', $string);

    	$temp = explode(' ', $string);
    	$bigram = "";
    	foreach ($temp as $key => $word) {
    		$index = $key + 1;

    		if ($index % 2 == 0 && $index != count($temp)) {
    			// add comma each 2 words
    			$bigram .= $word . ", ";
    		}
    		else if ($index == count($temp)) {
    			// if last word
    			$bigram .= $word;
    		}
    		else {
    			$bigram .= $word . " ";
    		}
    	}

    	$trigram = "";
    	foreach ($temp as $key => $word) {
    		$index = $key + 1;

    		if ($index % 3 == 0 && $index != count($temp)) {
    			// add comma each 3 words
    			$trigram .= $word . ", ";
    		}
    		else if ($index == count($temp)) {
    			// if last word
    			$trigram .= $word;
    		}
    		else {
    			$trigram .= $word . " ";
    		}
    	}

    	$result = [
    		"unigram" => $unigram,
    		"bigram"  => $bigram,
    		"trigram" => $trigram,
    	];

        return view('part1', ['result' => $result]);
    }

    /* show table with pattern */
    public function showTable()
    {
    	return view('table');
    }

    /*
     * encrypt string by shifting the alphabet order
     * A-Z = 65-90
     * a-z = 97-122
    */
    public function encrypt($string="DFHKNQ")
    {
    	// check if string only contain alphabet
    	if (preg_match('/[^a-zA-Z]/', $string) > 0) {
    		return [
    			"status" => "fail",
    			"message" => "String should consist of alphabet only"
    		];
		}

    	// split string to letters
    	$temp = str_split($string);
    	$encrypted = [];

    	foreach ($temp as $key => $letter) {
    		$index = $key + 1;

    		$order = ord($letter);	// convert letter to ASCII

    		// restrict new letter to only alphabet
    		$min = 65;
    		$max = 90;

    		// if lowercase
    		if ($order >= 97 && $order <= 122) {
    			$min = 97;
	    		$max = 122;
    		}
    		
    		if ($index % 2 == 1) {
    			// if index = odd, then plus
    			$encrypted_order = $order + $index;
    			
    			// if encrypted_order > z, then back to a
    			if ($encrypted_order > $max) {
    				$diff = $encrypted_order - $max;
    				$encrypted_order = $min + $diff - 1;
    			}
    		}
    		else {
    			// if index = even, then minus
    			$encrypted_order = $order - $index;

    			// if encrypted_order < a, then back to z
    			if ($encrypted_order < $min) {
    				$diff = $min - $encrypted_order;
    				$encrypted_order = $max - $diff + 1;
    			}
    		}

    		$encrypted_letter = chr($encrypted_order);	// convert ASCII to letter
    		array_push($encrypted, $encrypted_letter);
    	}

    	$encrypted_string = implode('', $encrypted);

    	$result = [
    		"original_string" => $string,
    		"encrypted_string" => $encrypted_string,
    	];

        return view('part1', ['result' => $result]);
    }

    public function searchWord($string="fghi")
    {
    	$array = [
			['f', 'g', 'h', 'i'],
			['j', 'k', 'p', 'q'],
			['r', 's', 't', 'u']
		];

		$search_result = $this->searchWordInArray($array, $string);

		$result = [
			"array" 		=> $array,
			"string" 		=> $string,
			"search_result" => $search_result ? "True" : "False"
		];

        return view('part1', ['result' => $result]);
    }

    /* search word in array of array of letters */
    private function searchWordInArray($array, $string)
    {
    	// check if array is array of array
    	if ( !(is_array($array) && is_array($array[0])) ) {
    		return "Input array is not valid";
    	}

    	$temp = str_split($string);
    	foreach ($temp as $key => $letter) {
    		$search = false;

    		foreach ($array as $key => $value) {
    			$search = in_array($letter, $value);
    			
    			// if letter already found, then break and continue to the next letter
    			if ($search == true) {
					break;
				}
    		}

    		// if 1 letter not found, then break and stop searching
			if ($search == false) {
				break;
			}
    	}

    	return $search;
    }
}
