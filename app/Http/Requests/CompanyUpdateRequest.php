<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class CompanyUpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'required|numeric',
            'name' => 'required',
            'email' => "required|email|unique:companies,email,{$this->id}",
            'logo' => 'sometimes|image|mimes:png|dimensions:ratio=1/1,min_width=100,min_height=100|max:2048', // 2 MB
            'website' => 'required|url',
        ];
    }
}
