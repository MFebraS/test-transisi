## About
This is a test web from Transisi. It contain logic test and simple CRUD. Use framework Laravel v 7.0 and database MySQL.

## How to Run
- Clone project from branch **master**
- Open terminal & go to project directory
- Run `composer install`
- Create new database
- Create .env file & update database name
- In terminal run:
	- `sudo chmod 777 -R storage` (Linux)
	- `php artisan key:generate`
	- `php artisan migrate`
	- `php artisan db:seed`
- Now you can run the app in browser
	- eg: http://localhost/test-transisi (whithout **public** url)

## Notes
- For Part 1 Test, some cases can accept parameter in url
	- eg: http://localhost/test-transisi-master/part-1/no-5/DFHKNQ
