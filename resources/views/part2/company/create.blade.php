@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="title">Create Company</div>
                    </div>
                </div>

                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-6">
                            <form method="POST" action="{{ route('company.store') }}" enctype="multipart/form-data">
                                @csrf

                                <div class="form-group">
                                    <label>Name *</label>
                                    <input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
                                </div>
                                <div class="form-group">
                                    <label>Email *</label>
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                </div>
                                <div class="form-group">
                                    <label>Logo *</label>
                                    <p class="text-muted">Image ratio should be 1:1, min dimensions is 100x100 px, max size is 2 MB</p>

                                    <img id="preview" src="" width="100" height="100" class="mb-3 img-thumbnail hidden">

                                    <input id="logo" type="file" class="form-control-file" name="logo" value="{{ old('logo') }}" accept="image/png" required>
                                </div>
                                <div class="form-group">
                                    <label>Website *</label>
                                    <input type="text" class="form-control" name="website" value="{{ old('website') }}" placeholder="https://mysite.com" required>
                                </div>

                                <div class="mt-4">
                                    <a href="{{ url()->previous() }}" class="btn btn-outline-secondary mr-2">Cancel</a>
                                    <input type="submit" value="Save" class="btn btn-primary">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            function readURL(input) {
                if (input.files && input.files[0]) {
                    var reader = new FileReader();

                    reader.onload = function(e) {
                        $('#preview').attr('src', e.target.result);
                    }
                    reader.readAsDataURL(input.files[0]); // convert to base64 string
                }
            }

            $('#logo').change(function() {
                readURL(this);
                $('#preview').show();
            });
        });
    </script>
@endsection
