@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="title">Detail Company</div>
                    </div>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <table class="custom-table">
                                <tbody>
                                    <tr>
                                        <td class="label">Name</td>
                                        <td class="semicolon">:</td>
                                        <td>{{ $data->name }}</td>
                                    </tr>
                                    <tr>
                                        <td class="label">Email</td>
                                        <td class="semicolon">:</td>
                                        <td>{{ $data->email }}</td>
                                    </tr>
                                    <tr>
                                        <td class="label">Logo</td>
                                        <td class="semicolon">:</td>
                                        <td>
                                            <img src="{{ asset('storage/app') .'/'. $data->logo }}" width="100" height="100">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label">Website</td>
                                        <td class="semicolon">:</td>
                                        <td>{{ $data->website }}</td>
                                    </tr>
                                    <tr>
                                        <td class="label">Employee(s)</td>
                                        <td class="semicolon">:</td>
                                        <td>
                                            @forelse($data->employees as $key => $employee)
                                                <div>{{ $employee->name }}</div>
                                            @empty
                                                -
                                            @endforelse
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            
                            <div class="mt-4">
                                <a href="{{ url()->previous() }}" class="btn btn-outline-primary">Back</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection