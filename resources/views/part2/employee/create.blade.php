@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="title">Create Employee</div>
                    </div>
                </div>

                <div class="card-body">
                    @if ($errors->any())
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif

                    <div class="row">
                        <div class="col-6">
                            <form method="POST" action="{{ route('employee.store') }}">
                                @csrf

                                <div class="form-group">
                                    <label>Name *</label>
                                    <input type="text" class="form-control" name="name" value="{{ old('name') }}" required>
                                </div>
                                <div class="form-group">
                                    <label>Email *</label>
                                    <input type="email" class="form-control" name="email" value="{{ old('email') }}" required>
                                </div>
                                <div class="form-group">
                                    <label>Company *</label>
                                    <select class="form-control" name="company_id">
                                        @forelse($companies as $key => $company)
                                            <option value="{{ $company->id }}">{{ $company->name }}</option>
                                        @empty
                                            <option>No option</option>
                                        @endforelse
                                    </select>
                                </div>

                                <div class="mt-4">
                                    <a href="{{ url()->previous() }}" class="btn btn-outline-secondary mr-2">Cancel</a>
                                    <input type="submit" value="Save" class="btn btn-primary">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection