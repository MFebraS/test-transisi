@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex justify-content-between align-items-center">
                        <div class="title">Employee</div>
                        <a href="{{ route('employee.create') }}" class="btn btn-primary">Create</a>
                    </div>
                </div>

                <div class="card-body">
                    @if(session('success'))
                        <div class="alert alert-success" role="alert">
                            {{ session('success') }}
                        </div>
                    @endif

                    @isset($data)
                        @php
                            $current_page = $data->currentPage() - 1;
                            $per_page = $data->perPage();
                        @endphp

                        <table class="table table-striped">
                            <thead>
                                <tr>
                                    <th class="no">No</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Company Name</th>
                                    <th>Last Modified</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($data as $key => $item)
                                    <tr>
                                        <td>{{ $current_page * $per_page + $key + 1 }}</td>
                                        <td>{{ $item->name }}</td>
                                        <td>{{ $item->email }}</td>
                                        <td>{{ $item->company->name }}</td>
                                        <td>{{ date('d-m-Y H:i:s', strtotime($item->updated_at)) }}</td>
                                        <td>
                                            <a href="{{ route('employee.detail', $item->id) }}" class="btn btn-sm btn-outline-primary mr-2">Detail</a>
                                            
                                            <a href="{{ route('employee.edit', $item->id) }}" class="btn btn-sm btn-info mr-2">Edit</a>

                                            <a href="#" class="btn btn-sm btn-danger btn-delete"    data-link="{{ route('employee.delete', $item->id) }}" data-name="{{ $item->name }}">Delete</a>
                                        </td>
                                    </tr>
                                @empty
                                    <tr>
                                        <td class="text-center" colspan="6">No data available</td>
                                    </tr>
                                @endforelse
                            </tbody>
                        </table>

                        <div class="d-flex flex-row-reverse mt-5">
                            {{ $data->links() }}
                        </div>
                    @endisset
                </div>
            </div>
        </div>
    </div>

    {{-- modal confirm delete --}}
    <div class="modal fade" id="modal-confirm-delete" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Confirm Delete</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>Are you sure want to delete <b id="delete-name"></b>?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-outline-secondary" data-dismiss="modal">Cancel</button>
                    <a href="#" id="btn-confirm-delete" class="btn btn-danger">Delete</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('js')
    <script type="text/javascript">
        $(document).ready(function () {
            // assign delete url & employee name
            $('.btn-delete').on('click', function () {
                var link = $(this).data("link");
                var name = $(this).data("name");

                $('#delete-name').text(name);
                $('#btn-confirm-delete').attr('href', link);

                // show the modal
                $('#modal-confirm-delete').modal('show');
            });

            // remove delete url & employee name when modal hidden
            $('#modal-confirm-delete').on('hidden.bs.modal', function (e) {
                $('#delete-name').text('');
                $('#btn-confirm-delete').attr('href', '#');
            });
        });
    </script>
@endsection
