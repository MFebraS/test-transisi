<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie-edge">

	<title>Part 1</title>

	<link rel="stylesheet" type="text/css" href="{{ asset('public/css/app.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ asset('public/css/styles.css') }}">

	@yield('css')
</head>

<body>
	<div class="home">
		<div class="row">
			{{-- sidebar --}}
			<div class="sidebar col-2 bg-primary">
				{{-- Part 1 --}}
				<h4 class="text-center border-bottom border-white pb-1">Part 1</h4>
				<nav>
					<ol class="flex-column">
						<li>
							<a href="{{ url('part-1/no-1') }}" class="pt-1 pb-1">Calculate Score</a>
						</li>
						<li>
							<a href="{{ url('part-1/no-2', 'TranSISI') }}" class="pt-1 pb-1">
								Calculate Lowercase
							</a>
						</li>
						<li>
							<a href="{{ url('part-1/no-3', 'Jakarta adalah ibukota negara Republik Indonesia') }}" class="pt-1 pb-1">
								Parse String
							</a>
						</li>
						<li>
							<a href="{{ url('part-1/no-4') }}" class="pt-1 pb-1">Show Table</a>
						</li>
						<li>
							<a href="{{ url('part-1/no-5', 'DFHKNQ') }}" class="pt-1 pb-1">
								Encrypt String
							</a>
						</li>
						<li>
							<a href="{{ url('part-1/no-6', 'fghi') }}" class="pt-1 pb-1">
								Search String
							</a>
						</li>
					</ol>
				</nav>

				{{-- Part 2 --}}
				<h4 class="text-center border-bottom border-white pb-1">Part 2</h4>
				<nav>
					<ol class="flex-column">
						<li>
							<a href="{{ url('part-2/login') }}" class="pt-1 pb-1">
								Login
							</a>
						</li>
					</ol>
				</nav>
			</div>

			{{-- content --}}
			<div class="content col offset-2">
				{{-- home --}}
				@if (Request::is('/'))
				<div class="pl-3 pr-3">
					<div class="d-flex justify-content-center align-items-center" style="height: 400px">
						<div class="text-center">
							<h3>Transisi Test</h3>
							<div>- M. Febra S -</div>
						</div>
					</div>
				</div>
				@endif
				{{-- end home --}}

				@isset($result)
				<div class="pl-3 pr-3">
					@if(is_array($result))
						<table class="table table-striped">
							<tbody>
								@foreach($result as $key => $item)
									<tr class="row">
										<td class="col-2">{{ ucwords(str_replace('_', ' ', $key)) }}</td>
										<td class="col">
											@if(is_array($item))
												@foreach($item as $k => $value)
													@if(is_array($value))
														<div>
															@foreach($value as $i => $v)
																{{ $v . ", " }}
															@endforeach
														</div>
													@else
														<div>{{ $value }}</div>
													@endif
												@endforeach
											@else
												{{ $item }}
											@endif
										</td>
									</tr>
								@endforeach
							</tbody>
						</table>
					@else
						<div class="p-3 rounded border border-secondary bg-light">
							{{ $result }}
						</div>
					@endif
				</div>
				@endisset

				@yield('table')
			</div>
		</div>
	</div>
</body>
</html>