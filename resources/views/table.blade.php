@extends('part1')

@section('css')
	<style type="text/css">
		.view-table{
			padding: 20px;
		}
		.block{
			background-color: #000;
			color: #fff;
		}
		.table{
			width: unset;
			background-color: #fff;
			border: 1px solid #ccc;
		}
		.table td{
			padding: 7px 15px;
			text-align: center;
		}
	</style>
@stop

@section('table')
	@php
		$row = 1;
		$black = 2;
		$white = 0;
		$next_black_gap = 1;
		$next_white_gap = 2;
		$last_black_gap = 2; 	// previous next_black_gap
	@endphp

	<div class="view-table">
		<table class="table">
			<tbody>
				@while($row <= 64)
					<tr>
						@for($i = 0; $i < 8; $i++)
							@php
								$number = $row + $i;
							@endphp

							<td class="{{ $black > 0 ? 'block' : '' }}">
								{{ $number }}
							</td>

							@php
								if ($black > 0) {
									$black--;
									if ($black == 0) {
										$white = $next_white_gap;
										// next_white_gap or white box have pattern 2 1 2 1
										if ($next_white_gap == 2) {
											$next_white_gap = 1;
										}
										else {
											$next_white_gap = 2;
										}
									}
								}
								else if ($white > 0) {
									$white--;
									if ($white == 0) {
										$black = $next_black_gap;

										// next_black_gap or black box have pattern 211 2 211 2 211
										if ($last_black_gap == 1 && $next_black_gap == 1 ||
											$last_black_gap == 1 && $next_black_gap == 2) {
											$last_black_gap = $next_black_gap;
											$next_black_gap = 2;
										}
										else {
											$last_black_gap = $next_black_gap;
											$next_black_gap = 1;
										}
									}
								}
							@endphp
						@endfor
					</tr>

					@php
						$row += 8;
					@endphp
				@endwhile
			</tbody>
		</table>
	</div>
@stop