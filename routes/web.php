<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('part1');
});

Route::prefix('/part-1')->group(function () {
	Route::get('no-1', 'Part1Controller@calculateScore');
	Route::get('no-2/{string?}', 'Part1Controller@calculateLowercase');
	Route::get('no-3/{string?}', 'Part1Controller@parseString');
	Route::get('no-4', 'Part1Controller@showTable');
	Route::get('no-5/{string?}', 'Part1Controller@encrypt');
	Route::get('no-6/{string?}', 'Part1Controller@searchWord');
	
});

Route::prefix('/part-2')->group(function () {
	Auth::routes();

	// redirect to company list
	Route::get('/', function () {
	    return redirect()->route('company.index');
	});

	Route::middleware('auth')->group(function () {
		// company route
		Route::prefix('company')->name('company.')->group(function () {
			Route::get('/', 'Part2\CompanyController@index')->name('index');
			Route::get('create', 'Part2\CompanyController@create')->name('create');
			Route::post('store', 'Part2\CompanyController@store')->name('store');
			Route::get('edit/{id}', 'Part2\CompanyController@edit')->name('edit');
			Route::post('update', 'Part2\CompanyController@update')->name('update');
			Route::get('detail/{id}', 'Part2\CompanyController@show')->name('detail');
			Route::get('delete/{id}', 'Part2\CompanyController@destroy')->name('delete');
		});

		// employee route
		Route::prefix('employee')->name('employee.')->group(function () {
			Route::get('/', 'Part2\EmployeeController@index')->name('index');
			Route::get('create', 'Part2\EmployeeController@create')->name('create');
			Route::post('store', 'Part2\EmployeeController@store')->name('store');
			Route::get('edit/{id}', 'Part2\EmployeeController@edit')->name('edit');
			Route::post('update', 'Part2\EmployeeController@update')->name('update');
			Route::get('detail/{id}', 'Part2\EmployeeController@show')->name('detail');
			Route::get('delete/{id}', 'Part2\EmployeeController@destroy')->name('delete');
		});
	});
});
